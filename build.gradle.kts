buildscript {
    val kotlinVersion by extra("1.4.21")
    val dokkaVersion by extra("0.9.17")
    val jacocoVersion by extra("0.8.4")
    val navigationVersion by extra("2.3.2")

    repositories {
        google()
        jcenter()
    }
    dependencies {
        // NOTE: This version is a bit lower due to INTELLIJ IDEA
        // https://stackoverflow.com/a/65864176/2333802
        classpath("com.android.tools.build", "gradle", "4.0.2")

        // Kotlin
        classpath("org.jetbrains.kotlin", "kotlin-gradle-plugin", kotlinVersion)
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlinVersion)

        // Firebase
        classpath("com.google.gms", "google-services", "4.3.4")
        classpath("com.google.firebase", "firebase-crashlytics-gradle", "2.4.1")

        // Navigation
        classpath("androidx.navigation", "navigation-safe-args-gradle-plugin", navigationVersion)

        // Testing
        classpath("de.mannodermaus.gradle.plugins", "android-junit5", "1.6.2.0")

        // Code coverage
        classpath("org.jacoco", "org.jacoco.core", jacocoVersion)

        // Documentation
        classpath("org.jetbrains.dokka", "dokka-android-gradle-plugin", dokkaVersion)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven {
            setUrl("https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/repos/")
        }
    }
}
