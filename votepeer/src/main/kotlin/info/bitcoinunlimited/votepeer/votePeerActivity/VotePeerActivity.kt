package info.bitcoinunlimited.votepeer.votePeerActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.election.MyFirebaseMessagingServiceLib.Companion.checkFirebaseMessagingToken
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
open class VotePeerActivity : AppCompatActivity(), VotePeerActivityView {
    private val viewModel: VotePeerActivityViewModel by viewModels {
        InjectorUtils.provideVotingActivityViewModelFactory(
            this,
            intent.getByteArrayExtra("privateKey")
                ?: throw Exception("Cannot get privateKey from intent")
        )
    }

    private val votingActivityViewIntent = VotePeerActivityViewIntent()

    override fun initState() = votingActivityViewIntent.initState
    override fun qrCodeRead() = votingActivityViewIntent.qrCodeRead
    override fun submitConnectionStatus() = votingActivityViewIntent.submitConnectionStatus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeConnectionStatus()
        viewModel.observeAuth()

        val privateKey = intent.getByteArrayExtra("privateKey")
            ?: throw Exception("Cannot get privateKey from intent")
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, privateKey)
        GlobalScope.launch {
            val walletService = WalletService(payDestination)
            AuthRepository.getInstance(walletService).signIn()

            checkFirebaseMessagingToken(payDestination)
        }

        viewModel.bindIntents(this)
        viewModel.initElectrumApiConnection()
        viewModel.observeElectrumApiExceptions()

        setContentView(R.layout.activity_pure_voting_votepeer_lib)
        initLayout(getTopLevelDestinations())
    }

    private fun initLayout(votePeerLibraryTopLevelDestinations: Set<Int>) {
        val toolbar: Toolbar? = findViewById(R.id.toolbar_votepeer_lib)
        val setSupportActionBar = intent.getBooleanExtra("setSupportActionBar", true)
        val setupActionBarWithNavController = intent.getBooleanExtra("setupActionBarWithNavController", true)
        val navController = findNavController(R.id.nav_host_fragment_votepeer_lib)
        val navView: NavigationView = findViewById(R.id.nav_view_votepeer_lib)

        if (setSupportActionBar) {
            setSupportActionBar(toolbar)
        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            votePeerLibraryTopLevelDestinations,
        )

        if (setupActionBarWithNavController) setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        findViewById<Toolbar>(R.id.toolbar_votepeer_lib).setNavigationOnClickListener {
            hideKeyBoard()
            navController.popBackStack()
        }
    }

    // TODO: Move to activityViewModel: Hide/Show state
    private fun hideKeyBoard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager ?: return
        imm.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    override fun onResume() {
        super.onResume()
        val toolbar: TextView? = findViewById(R.id.toolbar_error)
        toolbar?.visibility = View.GONE
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun observeConnectionStatus() {
        val isOnline = isOnline(applicationContext)
        val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(isOnline))
        connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(true))
            }

            override fun onLost(network: Network) {
                votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(false))
            }
        })
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result: IntentResult? = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && result != null) {
            if (result.contents == null) {
                println("result.contents == null")
            } else {
                val uri = result.contents
                viewModel.qrCodeRead(uri)
            }
        }
    }

    override fun render(state: VotePeerActivityViewState) {
        when (state) {
            is VotePeerActivityViewState.ConnectionStatus -> renderConnectionStatus(state)
            is VotePeerActivityViewState.AuthenticatedAnonymously -> { }
            is VotePeerActivityViewState.AuthenticatedWithCustomUser -> { }
            is VotePeerActivityViewState.VotePeerActivityError -> { renderError(state) }
            is VotePeerActivityViewState.QrScanner -> { renderQrScannerState(state) }
        }
    }

    private fun renderQrScannerState(state: VotePeerActivityViewState.QrScanner) {
        if (state.start)
            IntentIntegrator(this).initiateScan()
    }

    private fun renderConnectionStatus(connection: VotePeerActivityViewState.ConnectionStatus) {
        val isConnected = connection.isOnline
        val toolbar: TextView? = findViewById(R.id.toolbar_error)
        val electrumApiConnectionStatus: TextView? = findViewById(R.id.toolbar_electrum_api_connection_status)

        if (isConnected) {
            toolbar?.visibility = View.GONE
            electrumApiConnectionStatus?.visibility = View.VISIBLE
        } else {
            toolbar?.visibility = View.VISIBLE
            toolbar?.text = "OFFLINE"
            electrumApiConnectionStatus?.visibility = View.GONE
        }
    }

    private fun renderError(state: VotePeerActivityViewState.VotePeerActivityError) {
        val exception = state.exception
        val message = exception.localizedMessage ?: exception.message ?: ""
        MaterialAlertDialogBuilder(this)
            .setTitle("Something went wrong in MainActivity!")
            .setMessage(message)
            .setNeutralButton("ok") { _, _ ->
                // Do nothing
            }
            .show()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_votepeer_lib)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    companion object {
        @Suppress("unused")
        fun getTopLevelDestinations(): Set<Int> {
            return setOf(R.id.nav_elections)
        }
    }
}
