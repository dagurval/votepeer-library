package info.bitcoinunlimited.votepeer.votePeerActivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.network.QrCodeData
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import java.net.URI
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class VotePeerActivityViewModel(
    private val authRepository: AuthRepository,
    private val electrumAPI: ElectrumAPI,
    private val walletService: WalletService,
) : ViewModel() {
    private val privateKey = walletService.currentUser.secret ?: throw Exception("Cannot get private key")
    val address = walletService.currentUser.address.toString()
    val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, privateKey)
    val state = MutableStateFlow<VotePeerActivityViewState?>(null)
    private val coroutineErrorHandler = CoroutineExceptionHandler { _, throwable ->
        val exception = Exception(throwable)
        state.value = VotePeerActivityViewState.VotePeerActivityError(exception)
        throw exception
    }

    fun observeAuth() {
        authRepository.observeAuth()
        authRepository.authState.onEach {
            when (it) {
                is AuthState.AuthError -> {
                    val exception = it.exception
                    state.value = VotePeerActivityViewState.VotePeerActivityError(exception)
                }
                is AuthState.UnAuthenticated -> {}
                is AuthState.AuthenticatedAnonymously -> {}
                is AuthState.Authenticating -> {}
                is AuthState.Authenticated -> {}
            }
        }.flowOn(Dispatchers.IO + coroutineErrorHandler).launchIn(viewModelScope)
    }

    fun initElectrumApiConnection() = viewModelScope.launch(Dispatchers.IO + coroutineErrorHandler) {
        electrumAPI.connect()
    }

    fun bindIntents(view: VotePeerActivityView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.submitConnectionStatus().onEachEvent { connection ->
            setConnectionStatus(connection)
        }.launchIn(viewModelScope)
    }

    fun observeElectrumApiExceptions() {
        ElectrumAPI.exception.onEach {
            if (it != null) {
                state.value = VotePeerActivityViewState.VotePeerActivityError(it)
            }
        }.flowOn(Dispatchers.IO + coroutineErrorHandler).launchIn(viewModelScope)
    }

    fun qrCodeRead(rawUri: String) = viewModelScope.launch(Dispatchers.IO + coroutineErrorHandler) {
        val qrCodeData = parseUri(rawUri)
        val signedChallenge = walletService.signChallenge(qrCodeData.chal)
        authRepository.identify(signedChallenge, qrCodeData)
    }

    private fun parseUri(rawUri: String): QrCodeData {
        lateinit var op: String
        lateinit var chal: String
        lateinit var cookie: String
        val uri = URI(rawUri)
        val rawQuery = uri.rawQuery
        val queries = rawQuery.split("&")

        queries.forEach { query ->
            val querySplit = query.split("=")
            val queryParameter = querySplit[0]
            val queryValue = querySplit[1]

            when (queryParameter) {
                "op" -> op = queryValue
                "chal" -> chal = queryValue
                "cookie" -> cookie = queryValue
            }
        }

        return QrCodeData(
            op,
            chal,
            cookie
        )
    }

    fun startQrScanner() {
        state.value = VotePeerActivityViewState.QrScanner(start = true)
        state.value = VotePeerActivityViewState.QrScanner(start = false)
    }

    private fun setConnectionStatus(
        connection: VotePeerActivityViewState.ConnectionStatus
    ) = viewModelScope.launch(Dispatchers.Main + coroutineErrorHandler) {
        state.value = connection
    }
}
