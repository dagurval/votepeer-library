package info.bitcoinunlimited.votepeer.votePeerActivity

@Suppress("unused")
sealed class VotePeerActivityViewState {
    data class ConnectionStatus(
        val isOnline: Boolean
    ) : VotePeerActivityViewState()

    data class QrScanner(
        val start: Boolean
    ) : VotePeerActivityViewState()

    data class AuthenticatedAnonymously(
        val currentUserId: String
    ) : VotePeerActivityViewState()

    data class AuthenticatedWithCustomUser(
        val currentUserId: String
    ) : VotePeerActivityViewState()

    data class VotePeerActivityError(
        val exception: Exception
    ) : VotePeerActivityViewState()
}
