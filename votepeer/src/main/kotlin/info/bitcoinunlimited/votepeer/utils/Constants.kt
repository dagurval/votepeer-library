package info.bitcoinunlimited.votepeer.utils

const val CONTENT_RECYCLER_VIEW_POSITION = "contentRecyclerViewPosition"

/**
 * Constants for VotePeer
 * @Firebase
 * Firebase-related paths
 * @Functions
 * Serverless node-js endpoints
 */
object Constants {
    const val RequestChallenge = "request_challenge"
    const val FirebaseRegion = "europe-west1"
    const val EndpointUrl: String = "https://europe-west1-voter-6700b.cloudfunctions.net/"
}
