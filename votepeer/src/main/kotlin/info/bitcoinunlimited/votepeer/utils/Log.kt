package info.bitcoinunlimited.votepeer.utils

const val TAG_ELECTRUM = "Electrum"
const val TAG_VOTE = "Vote"
const val TAG_FIREBASE = "Firebase"
