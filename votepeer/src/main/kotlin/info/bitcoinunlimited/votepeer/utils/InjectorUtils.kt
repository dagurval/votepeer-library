package info.bitcoinunlimited.votepeer.utils

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionRepository
import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailViewModel
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewModelFactory
import info.bitcoinunlimited.votepeer.election.result.ElectionResultViewModel
import info.bitcoinunlimited.votepeer.election.room.ElectionDatabase
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivityViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
object InjectorUtils {

    fun provideElectionMasterViewModelFactory(
        fragment: Fragment,
        payDestination: PayDestination
    ): ElectionMasterViewModelFactory {
        val context = fragment.requireContext()
        val electionRepository = getElectionRepository(context)
        val electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)
        val walletService = WalletService(payDestination)
        val authRepository = AuthRepository.getInstance(walletService)

        return ElectionMasterViewModelFactory(
            fragment,
            electionRepository,
            authRepository,
            walletService,
            electrumAPI,
            payDestination
        )
    }

    fun provideElectionDetailViewModelFactory(
        election: Election,
        fragment: Fragment,
        privateKey: ByteArray
    ): ElectionDetailViewModel.ElectionDetailViewModelFactory {
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, privateKey)
        val context = fragment.requireContext()
        val chainSelector: ChainSelector = ChainSelector.BCHMAINNET
        val electrumRepository = getElectrumRepository(chainSelector)
        val electionRepository = getElectionRepository(context)
        val walletService = WalletService(payDestination)

        return ElectionDetailViewModel.ElectionDetailViewModelFactory(
            election,
            walletService,
            electionRepository,
            electrumRepository
        )
    }

    fun provideElectionResultViewModelFactory(
        election: Election
    ): ElectionResultViewModel.ElectionResultViewModelFactory {
        val electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)

        return ElectionResultViewModel.ElectionResultViewModelFactory(
            election,
            electrumAPI
        )
    }

    fun provideVotingActivityViewModelFactory(owner: SavedStateRegistryOwner, privateKey: ByteArray): VotePeerActivityViewModelFactory {
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, privateKey)
        val walletService = WalletService(payDestination)
        val authRepository = AuthRepository.getInstance(walletService)
        val electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)

        return VotePeerActivityViewModelFactory(authRepository, electrumAPI, walletService, owner)
    }

    private fun getElectionRepository(context: Context): ElectionRepository {
        val electionDao = ElectionDatabase.getInstance(context.applicationContext).electionDao()
        return ElectionRepository.getInstance(electionDao)
    }

    private fun getElectrumRepository(chainSelector: ChainSelector): ElectrumAPI {
        return ElectrumAPI.getInstance(chainSelector)
    }
}
