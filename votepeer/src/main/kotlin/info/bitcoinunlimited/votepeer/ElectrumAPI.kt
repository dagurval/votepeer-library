package info.bitcoinunlimited.votepeer

import android.util.Log
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionOption
import info.bitcoinunlimited.votepeer.election.Tally
import info.bitcoinunlimited.votepeer.utils.TAG_ELECTRUM
import info.bitcoinunlimited.votepeer.utils.TAG_VOTE
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import java.lang.Thread.sleep
import java.net.UnknownHostException
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.scheduleAtFixedRate
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectrumAPI(private val chain: ChainSelector) {
    val connectionState = MutableStateFlow<ElectrumApiConnectionState?>(null)

    private var cli: ElectrumClient? = null
    private var cliMutex = Mutex()
    private var bestBlockHeader: BlockHeader? = null
    private var bestBlockHeaderMutex: Mutex = Mutex()

    /**
     * Task for sending ping request to the server at 1 minute interval.
     */
    private var pingTask: TimerTask? = null
    private val ELECTRUM_SERVERS = arrayOf(
        arrayOf("bitcoincash.network", 50001),
        arrayOf("electroncash.de", 50001),
        arrayOf("bch.imaginary.cash", 50001),
        arrayOf("electroncash.dk", 50001),
        arrayOf("electrum.imaginary.cash", 50001)
    )

    /**
     * Attempt connection to electrum servers. Tries multiple servers.
     *
     * Requires cliMutex lock.
     *
     * Returns false if we were already connected, true if we made a new connection and raises error if we failed to connect.
     */
    // TODO: move to suspendCoroutine
    suspend fun connect(): Boolean {
        val pingErrorHandler = CoroutineExceptionHandler { _, exception ->
            println(exception)
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception(exception))
            throw exception
        }
        if (cli != null) {
            return false
        }
        if (chain != ChainSelector.BCHMAINNET) {
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception("NYI. Only BCH mainnet is supported."))
        }

        pingTask = null

        for (server in ELECTRUM_SERVERS) {
            val host = server[0] as String
            val port = server[1] as Int
            try {
                connectionState.value = ElectrumApiConnectionState.Connecting(host, port)
                Log.i(TAG_ELECTRUM, "Connecting to $host:$port ...")
                cli = ElectrumClient(chain, host, port)
                cli!!.start()
                val version = cli!!.version().toString()
                Log.i(TAG_ELECTRUM, "Connected to $host:$port. Server version $version")
                connectionState.value = ElectrumApiConnectionState.Connected(host, port)
                break
            } catch (e: java.net.SocketTimeoutException) {
                Log.w(TAG_ELECTRUM, "Electrum timeout: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                continue
            } catch (e: java.net.ConnectException) {
                Log.w(TAG_ELECTRUM, "Electrum connection error: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                continue
            } catch (e: UnknownHostException) {
                Log.w(TAG_ELECTRUM, "Electrum UnknownHostException: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                continue
            } catch (e: ElectrumRequestTimeout) {
                val message = "Connected to $host:$port, but server did not respond to version request. Disconnecting."
                Log.w(TAG_ELECTRUM, message)
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception(message))
                cli!!.stop()
                cli = null
            }
        }

        if (cli == null) {
            val exception = Exception("Failed to connect to electrum servers.")
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
        }

        // Keep connection alive sending ping request at 1 minute interval.
        // TODO: Error handling

        pingTask = Timer("electrum ping", true).scheduleAtFixedRate(100, 60 * 1000) {
            GlobalScope.launch(Dispatchers.IO + pingErrorHandler) { this@ElectrumAPI.ping() }
        }

        return true
    }

    private suspend fun waitForTip() {
        var attempts = 0
        while (true) {
            if (attempts++ > 100) {
                Log.w(TAG_ELECTRUM, "Failed to receive initial block header from electrum server")
                throw ElectrumRequestTimeout()
            }
            bestBlockHeaderMutex.withLock {
                if (bestBlockHeader != null) {
                    return
                }
            }
            sleep(50)
        }
    }

    // requires cliMutex lock
    private suspend fun startSubscriptions() {
        if (cli == null) {
            throw Error("Connection required for initiating subscription")
        }
        // Subscribe to block header
        cli?.let {
            it.subscribeHeaders { header ->
                GlobalScope.launch(Dispatchers.IO) {
                    bestBlockHeaderMutex.withLock {
                        bestBlockHeader = header
                    }
                }
            }
        }
    }

    /**
     * Calls a electrum client method. Connects if disconnected.
     */
    private suspend fun <T> withCli(action: suspend (ElectrumClient) -> T): T {
        cliMutex.withLock {
            for (attempt in 1..10) {
                try {
                    val connected = connect()
                    if (connected) {
                        startSubscriptions()
                    }
                    waitForTip()
                    return action(cli!!)
                } catch (e: ElectrumRequestTimeout) {
                    val exception = Exception("Electrum request attempt #$attempt timed out. Re-connecting.")
                    connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
                    cli!!.stop()
                    cli = null
                } catch (exception: Exception) {
                    connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
                    throw exception
                }
            }
            val exception = Exception("Failed to submit request to electrum server")
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
            throw exception
        }
    }

    /**
     * Fetches an unused utxo from the contract. If no such utxo exists, creates one by spending coins from identity utxos.
     */
    suspend fun getOrCreateContractCoin(
        chain: ChainSelector,
        identity: PayDestination,
        contract: TwoOptionVoteContract
    ): BCHspendable {
        // First check if contract has coins
        val contractAddress = contract.address(chain)
        val contractCoins = listUnspent(contractAddress)
        val dust = 546
        for (c in contractCoins) {
            if (c.amount >= dust + TwoOptionVoteContract.CAST_FEE) {
                c.secret = identity.secret
                return c
            }
        }

        // No coins in contract, so we send a coin to the contract
        var fundCoin: BCHspendable? = null
        val minInput = TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE
        for (c in listUnspent(identity)) {
            if (c.amount >= minInput) {
                fundCoin = c
                break
            }
        }
        if (fundCoin == null) {
            error("Not enough funds to cast a vote")
        }
        val fundInput = BCHinput(chain, fundCoin, BCHscript(chain))
        val fundTx = contract.fundContract(chain, fundInput, identity)
        val txHash = broadcast(fundTx)
        Log.i(TAG_VOTE, "Contract funded in tx $txHash")
        if (txHash != fundTx.calcHash().toHex()) {
            Log.w(TAG_ELECTRUM, "Expected fund tx hash ${fundTx.calcHash()}, but electrum returned $txHash")
        }

        val index = 0
        val newCoin = BCHspendable(chain, fundTx.calcHash(), index, fundTx.outputs[index].amount)
        newCoin.priorOutScript = fundTx.outputs[0].script
        newCoin.secret = identity.secret
        return newCoin
    }

    /**
     * Get server version
     */
    suspend fun version(): Pair<String, String> {
        return withCli {
            return@withCli it.version()
        }
    }

    /**
     * Get spendable coins for this destination.
     *
     * The secret in destination object will be passed on to the
     * BCHspendable instances.
     */
    suspend fun listUnspent(destination: PayDestination): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(destination)
        }
    }

    /**
     * Gets spendable coins for this destination.
     *
     * The BCHspendable instances will not contain the private key to spend the coins.
     */
    suspend fun listUnspent(address: PayAddress): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(address)
        }
    }

    /**
     * Get balance
     * **/
    @Suppress("unused")
    suspend fun getBalance(address: PayAddress): ElectrumClient.BalanceResult {
        return withCli {
            return@withCli it.getBalance(address)
        }
    }

    /**
     * Broadcast a transaction
     */
    suspend fun broadcast(tx: BCHtransaction): String {
        return withCli {
            return@withCli it.sendTx(tx.BCHserialize(SerializationType.NETWORK).flatten())
        }
    }

    /**
     * Fetch a transaction
     */
    suspend fun getTransaction(txid: Hash256): BCHtransaction {
        return withCli {
            return@withCli it.getTx(txid)
        }
    }

    suspend fun ping() {
        return withCli {
            return@withCli it.ping()
        }
    }

    /**
     * Get the chain tip
     */
    private suspend fun getBestBlockHeader(): BlockHeader? {
        bestBlockHeaderMutex.withLock {
            return bestBlockHeader
        }
    }

    suspend fun getLatestBlockHeight() = getBestBlockHeader()?.height ?: 0

    // Get human readable version of end time
    suspend fun getEndTimeHuman(endHeight: Long): String {
        getBestBlockHeader()?.let {
            val currentHeight = it.height

            if (currentHeight > endHeight) {
                return "Has ended"
            }

            val remaining = (endHeight - currentHeight) * 600
            val minutes = remaining % 3600 / 60
            val hours = remaining % 86400 / 3600
            val days = remaining / 86400

            val prefix = "Ends in"
            val postfix = "(at height $endHeight)"
            if (days != 0L) {
                return "$prefix $days day(s) and $hours hour(s) $postfix"
            }
            if (hours != 0L) {
                return "$prefix $hours hour(s) and $minutes minute(s) $postfix"
            }
            return "$prefix $minutes minute(s) $postfix"
        }

        // We don't know the current height
        return "Ends at blockchain height $endHeight"
    }

    suspend fun getTally(election: Election): Tally {

        val contracts = election.participants.map {
            WalletService.createContract(election, it)
        }

        var optionA = 0
        var optionB = 0
        var blank = 0
        var spoilt = 0
        var notVoted = 0

        Log.w(TAG_VOTE, "Contacts" + contracts.toString())

        for (c in contracts) {
            val vote = try {
                val voteTx = fetchVoteTransaction(c.address(ChainSelector.BCHMAINNET), election.endHeight)

                if (voteTx != null) {
                    WalletService.getVoteOption(c, election, voteTx)
                } else {
                    // User has not vote yet.
                    null
                }
            } catch (e: Exception) {
                Log.w(TAG_VOTE, e)
                // TODO: Too generic, error can be caused by something different than spoilt
                spoilt += 1
                continue
            }
            if (vote == null) {
                notVoted += 1
                continue
            }
            when (vote.option) {
                ElectionOption.OPTION_A -> optionA += 1
                ElectionOption.OPTION_B -> optionB += 1
                ElectionOption.BLANK -> blank += 1
            }
        }
        return Tally(election.id, optionA, optionB, blank, notVoted, spoilt)
    }

    /**
     * Fetches a vote transaction from the blockchain if there is one, otherwise null.
     * Raises error if there vote is spoilt.
     */
    suspend fun fetchVoteTransaction(contractAddress: PayAddress, endHeight: Long): BCHtransaction? {

        val txs = getSpendingTransactions(contractAddress, endHeight).sortedByDescending {
            it.first
        }
        var bestMatch: Pair<Long, BCHtransaction>? = null
        Log.w(TAG_VOTE, txs.toString())
        for (tx in txs) {
            val height = tx.first
            if (bestMatch == null) {
                bestMatch = tx
                continue
            }

            if (height == bestMatch.first) {
                // Oldest vote is at same height. The vote is spoilt.
                error("Multiple votes, vote is spoilt")
            }

            if (isInMempool(bestMatch.first) && isInMempool(height)) {
                // Same as above, oldest vote is in mempool and there are multiple votes. Vote is spoilt.
                // Technically they may confirm at different height, but it's unlikely.
                error("Multiple votes, vote is spoilt")
            }

            println(bestMatch)
            // Vote is not spoilt.
            return bestMatch.second
        }
        println(bestMatch)
        return bestMatch?.second
    }

    private fun isInMempool(confirmHeight: Long): Boolean {
        return confirmHeight < 1
    }

    /**
     * Get transactions that *spend* from the given address.
     *
     * Returns list of height, tx
     *
     * Height is 0 or -1 if the transaction is unconfirmed.
     */
    private suspend fun getSpendingTransactions(contractAddress: PayAddress, endHeight: Long?): List<Pair<Long, BCHtransaction>> {
        val txs = mutableListOf<Pair<Long, BCHtransaction>>()

        return withCli { cli ->
            val tipHeight = getBestBlockHeader()
            if (endHeight != null && tipHeight == null) {
                error("blockchain tip is not available")
            }

            val scripthash = contractAddress.outputScript().scriptHash()
            val history = cli.getHistory(scripthash)
            for (h in history) {
                if (endHeight != null) {
                    if (h.first > endHeight) {
                        continue
                    }
                }
                val tx = cli.getTx(h.second)
                if (hasInputFrom(tx, contractAddress)) {
                    txs.add(Pair(h.first.toLong(), tx))
                }
            }
            println("HI")
            return@withCli txs.toList()
        }
    }

    /**
     * Check if transaction spends from address in tx
     */
    private fun hasInputFrom(tx: BCHtransaction, address: PayAddress): Boolean {
        for (i in tx.inputs) {
            val txId = i.spendable.outpoint.txid
            val idx: Long = i.spendable.outpoint.getTransactionOrder()
            if (inputSpentTo(address, idx, txId)) {
                return true
            }
        }
        return false
    }

    /**
     * If input was spent to address
     */
    private fun inputSpentTo(address: PayAddress, idx: Long, txId: Hash256): Boolean {
        val tx = cli!!.getTx(txId)
        return tx.outputs[idx.toInt()].script.contentEquals(address.outputScript())
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectrumAPI? = null

        fun getInstance(chain: ChainSelector) =
            instance ?: synchronized(this) {
                instance ?: ElectrumAPI(chain).also { instance = it }
            }

        // For coRoutine Errors
        val exception: MutableStateFlow<Exception?> = MutableStateFlow(null)
    }
}
