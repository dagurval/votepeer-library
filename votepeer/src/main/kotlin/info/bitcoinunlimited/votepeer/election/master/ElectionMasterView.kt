package info.bitcoinunlimited.votepeer.election.master

import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface ElectionMasterView {
    /**
     * Intent to load the current ElectionMaster state
     *
     * @return A flow that inits the current ElectionMasterViewState
     */
    fun initState(): Flow<Event<Boolean>>

    fun initElectrumApiState(): Flow<Event<Boolean>>

    fun initAuthState(): Flow<Event<Boolean>>

    fun refreshSwiped(): Flow<Event<ElectionMasterViewIntent.SwipeToRefresh?>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: ElectionMasterViewState)

    fun renderElectrumApiState(renderElectrumApiState: ElectrumApiConnectionState)

    fun renderAuthState(state: AuthState)
}
