package info.bitcoinunlimited.votepeer.election.room

import androidx.room.TypeConverter
import bitcoinunlimited.libbitcoincash.FromHex
import bitcoinunlimited.libbitcoincash.toHex

class ElectionConverters {
    @TypeConverter
    fun toByteArray(data: String): Array<ByteArray> {
        return data.split(";").map {
            it.FromHex()
        }.toTypedArray()
    }

    @TypeConverter
    fun fromByteArray(data: Array<ByteArray>): String {
        return data.joinToString(";") {
            it.toHex()
        }
    }
}
