package info.bitcoinunlimited.votepeer.election

import android.util.Log
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.TwoOptionVote
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.election.room.ElectionDao
import info.bitcoinunlimited.votepeer.utils.TAG_FIREBASE
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class ElectionRepository(private val electionDao: ElectionDao) {
    suspend fun getElectionsNetwork(currentUserId: String): List<Election> {
        val electionSnapshotList = Firebase.firestore.collection("election")
            .whereArrayContains("participantAddresses", currentUserId)
            .get()
            .await()

        val electionList = electionSnapshotList.mapNotNull { electionSnap ->
            try {
                val participantsRaw = electionSnap.get("participantAddresses") as ArrayList<String>
                val participants = TwoOptionVote.sortByteArray(
                    participantsRaw.map {
                        getPKHFromAddress(it)
                    }.toTypedArray()
                )
                Election(
                    id = electionSnap.id,
                    adminAddress = electionSnap.getString("adminAddress") as String,
                    optionA = electionSnap.getString("optionA") as String,
                    optionB = electionSnap.getString("optionB") as String,
                    description = electionSnap.getString("description") as String,
                    endHeight = electionSnap.getLong("endHeight") as Long,
                    salt = electionSnap.getString("salt") as String,
                    participants = participants,
                    createdAt = electionSnap.getTimestamp("createdAt")!!.seconds,
                    modifiedAt = electionSnap.getTimestamp("modifiedAt")!!.seconds
                )
            } catch (e: Exception) {
                Log.e(TAG_FIREBASE, "Failed to parse entry in election list, ignoring: $e")
                null
            }
        }
        return electionList
    }

    fun getElectionsFromDisk(): List<Election> {
        return electionDao.getMainFeedRoom()
    }

    suspend fun saveElectionsToDisk(elections: List<Election>) = suspendCoroutine<Any?> {
        GlobalScope.launch(Dispatchers.IO) {
            electionDao.insert(elections)
            it.resume(null)
        }
    }

    fun clearElections() = electionDao.clearElections()

    private fun getPKHFromAddress(address: String): ByteArray {
        return PayAddress(address).data
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectionRepository? = null

        fun getInstance(electionDao: ElectionDao) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: ElectionRepository(
                            electionDao
                        )
                            .also { instance = it }
                }
    }
}
