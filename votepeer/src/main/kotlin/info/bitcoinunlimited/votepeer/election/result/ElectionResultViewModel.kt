package info.bitcoinunlimited.votepeer.election.result

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionResultViewModel(
    private var election: Election,
    private val electrumAPI: ElectrumAPI,
) : ViewModel() {
    private val state = MutableStateFlow<ElectionResultState?>(null)

    fun bindIntents(view: ElectionResultView) {
        state.value = ElectionResultState.LoadingTally(election)
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.updateResults().onEachEvent { vote ->
            refreshResults(vote, election)
        }.launchIn(viewModelScope)
    }

    internal fun fetchElectionResultNetwork() = viewModelScope.launch(Dispatchers.IO) {
        try {
            val tally = electrumAPI.getTally(election)
            state.value = ElectionResultState.HasTally(election, tally)
        } catch (exception: IllegalStateException) {
            viewModelScope.launch {
                val errorMessage = exception.message
                state.value = ElectionResultState.Error(errorMessage ?: "ERROR")
            }
        }
    }

    private fun refreshResults(
        event: ElectionResultIntent.RefreshResults,
        election: Election
    ) = viewModelScope.launch(Dispatchers.IO) {
        state.value = ElectionResultState.LoadingTally(election)
        fetchElectionResultNetwork()
    }

    class ElectionResultViewModelFactory(
        private val election: Election,
        private val electrumAPI: ElectrumAPI
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ElectionResultViewModel::class.java))
                return ElectionResultViewModel(
                    election,
                    electrumAPI,
                ) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}
