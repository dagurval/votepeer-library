package info.bitcoinunlimited.votepeer.election

import android.annotation.SuppressLint
import android.util.Log
import bitcoinunlimited.libbitcoincash.PayDestination
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.utils.TAG_VOTE
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
@SuppressLint("MissingFirebaseInstanceTokenRefresh")
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
open class MyFirebaseMessagingServiceLib : FirebaseMessagingService() {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG_NOTIFICATIONS, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG_NOTIFICATIONS, "Message data payload: ${remoteMessage.data}")

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }
        }
    }
    // [END receive_message]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]
        // val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        // WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG_NOTIFICATIONS, "Short lived task is done.")
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    companion object {
        const val TAG_NOTIFICATIONS = "MyFirebaseMsgService"

        private val handler = CoroutineExceptionHandler { coroutineContext, exception ->
            Log.e(TAG_VOTE, exception.message ?: "Error in MyFirebaseMessaegingServiceLib")
            throw exception
        }

        @Suppress("MemberVisibilityCanBePrivate")
        fun setAndroidNotificationToken(
            payDestination: PayDestination?,
            firebaseMessagingToken: String
        ) = GlobalScope.launch(Dispatchers.IO + handler) {
            payDestination ?: return@launch
            val currentUserId = payDestination.address.toString()
            if (isAuthenticated(payDestination)) {
                val data = hashMapOf("androidNotificationToken" to firebaseMessagingToken)
                Firebase.firestore.collection("user").document(currentUserId).set(data)
            }
        }

        @Suppress("MemberVisibilityCanBePrivate")
        fun isAuthenticated(currentUser: PayDestination): Boolean {
            val walletService = WalletService(currentUser)
            val authRepository = AuthRepository.getInstance(walletService)
            val isAnonymous = authRepository.isSignedIdAnonymously()
            val authState = authRepository.authState.value

            return !isAnonymous && authState is AuthState.Authenticated
        }

        internal fun checkFirebaseMessagingToken(currentUser: PayDestination) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(
                OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(TAG_NOTIFICATIONS, "Fetching FCM registration token failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new FCM registration token
                    val token = task.result ?: throw Exception("Cannot get token in checkFirebaseMessagingToken!")

                    setAndroidNotificationToken(currentUser, token)
                }
            )
        }
        // TODO: Make concurrency-safe
        // NOTE: Only use this if you have to
        lateinit var currentUserCompanion: PayDestination
    }
}
