package info.bitcoinunlimited.votepeer.election.detail

import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.Vote

sealed class ElectionDetailState {
    data class ElectionDetailIsLoading(
        val election: Election
    ) : ElectionDetailState()

    data class IsVoting(
        val election: Election
    ) : ElectionDetailState()

    object CastingVote : ElectionDetailState()

    data class HasVoted(
        val election: Election,
        val voteTransactionId: String
    ) : ElectionDetailState()

    data class VoteExpired(
        val election: Election,
        val hasVoted: Boolean,
        val vote: Vote?
    ) : ElectionDetailState()

    data class ErrorState(
        val message: String
    ) : ElectionDetailState()

    data class VoteSpoilt(
        val election: Election
    ) : ElectionDetailState()
}
