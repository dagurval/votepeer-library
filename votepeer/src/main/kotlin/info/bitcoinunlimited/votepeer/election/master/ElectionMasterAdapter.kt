package info.bitcoinunlimited.votepeer.election.master

import android.view.LayoutInflater
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import info.bitcoinunlimited.votepeer.databinding.ItemElectionBinding.inflate
import info.bitcoinunlimited.votepeer.election.Election
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterAdapter() : ListAdapter<Election, ElectionMasterAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val election = getItem(position) ?: return
        holder.bind(createOnClickListener(election, holder), election)
    }

    private fun createOnClickListener(election: Election, holder: ViewHolder) = OnClickListener { _ ->

        val navigateToElectionDetailAction =
            ElectionMasterFragmentDirections.actionElectionMasterFragmentToElectionDetailFragment(election.getTitle(), election)
        holder.itemView.findNavController().navigate(navigateToElectionDetailAction)
    }

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(onClickListener: OnClickListener, election: Election) {
            binding.setVariable(BR.data, election)
            binding.setVariable(BR.clickListener, onClickListener)
            binding.setVariable(BR.timeUntilEnd, election.timeUntilEnd)
            binding.setVariable(BR.vote, election.vote)
            binding.setVariable(BR.error, election.votingError)
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Election>() {
            override fun areItemsTheSame(oldElection: Election, newElection: Election): Boolean =
                oldElection.id == newElection.id

            override fun areContentsTheSame(oldElection: Election, newElection: Election): Boolean =
                oldElection == newElection
        }
    }
}
