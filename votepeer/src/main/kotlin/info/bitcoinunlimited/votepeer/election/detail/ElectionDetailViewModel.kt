package info.bitcoinunlimited.votepeer.election.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.BCHinput
import bitcoinunlimited.libbitcoincash.BCHscript
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionOption
import info.bitcoinunlimited.votepeer.election.ElectionRepository
import info.bitcoinunlimited.votepeer.election.Vote
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailViewModel(
    private val election: Election,
    private val walletService: WalletService,
    private val electionRepository: ElectionRepository,
    private val electrumAPI: ElectrumAPI
) : ViewModel() {
    private val state = MutableStateFlow<ElectionDetailState?>(null)
    private val currentUser = walletService.currentUser

    fun bindIntents(view: ElectionDetailView) {
        state.value = ElectionDetailState.ElectionDetailIsLoading(election)
        checkElectionState()
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.submitVote().onEachEvent { vote ->
            castVote(ChainSelector.BCHMAINNET, currentUser, vote.vote, election)
        }.launchIn(viewModelScope)
    }

    internal fun checkElectionState() = viewModelScope.launch(Dispatchers.IO) {
        val currentHeight = electrumAPI.getLatestBlockHeight()

        try {
            val currentUserPkh = walletService.getCurrentUserPkh()
            val contract = WalletService.createContract(election, currentUserPkh)
            val voteTx = electrumAPI.fetchVoteTransaction(contract.address(ChainSelector.BCHMAINNET), election.endHeight)
            if (voteTx != null) {
                val currentUserVote = walletService.getCurrentUserVote(election, voteTx)
                setElectionState(currentHeight, election, currentUserVote)
            } else {
                setElectionState(currentHeight, election, null)
            }
        } catch (exception: IllegalStateException) {
            val errorMessage = exception.message

            if (errorMessage == "Multiple votes, vote is spoilt") {
                val spoiltElection = election.copy(voteSpoilt = true)
                state.value = ElectionDetailState.VoteSpoilt(spoiltElection)

                viewModelScope.launch(Dispatchers.IO) {
                    electionRepository.saveElectionsToDisk(listOf(spoiltElection))
                }
            } else {
                state.value = ElectionDetailState.ErrorState(errorMessage ?: "ERROR")
            }
        }
    }

    private fun setElectionState(
        currentHeight: Long,
        election: Election,
        currentUserVote: Vote?
    ) = viewModelScope.launch {
        val hasVoted = currentUserVote != null

        if (currentHeight >= election.endHeight && currentUserVote != null) {
            state.value = ElectionDetailState.VoteExpired(election, hasVoted, currentUserVote)
        } else if (currentHeight >= election.endHeight) {
            state.value = ElectionDetailState.VoteExpired(election, hasVoted, null)
        } else if (hasVoted && currentUserVote != null) {
            setElectionVotedState(currentUserVote)
        } else {
            state.value = ElectionDetailState.IsVoting(election)
        }
    }

    private fun setElectionVotedState(currentUserVote: Vote) = viewModelScope.launch {
        val optionSelected = currentUserVote.option
        val voteTransaction = currentUserVote.voteTransaction.hash.toString()

        when (optionSelected) {
            ElectionOption.OPTION_A -> {
                state.value = ElectionDetailState.HasVoted(election.copy(vote = election.optionA), voteTransaction)
            }
            ElectionOption.OPTION_B -> {
                state.value = ElectionDetailState.HasVoted(election.copy(vote = election.optionB), voteTransaction)
            }
            ElectionOption.BLANK -> {
                state.value = ElectionDetailState.HasVoted(election.copy(vote = "blank"), voteTransaction)
            }
        }
    }

    private suspend fun castVote(
        chain: ChainSelector,
        identity: PayDestination,
        optionSelected: ElectionOption,
        election: Election
    ) = viewModelScope.launch(Dispatchers.IO) {
        state.value = ElectionDetailState.CastingVote

        val voteOption = when (optionSelected) {
            ElectionOption.OPTION_A -> election.getOptionAHash()
            ElectionOption.OPTION_B -> election.getOptionBHash()
            ElectionOption.BLANK -> TwoOptionVoteContract.BLANK_VOTE
        }

        val electionWithVote = when (optionSelected) {
            ElectionOption.OPTION_A -> election.copy(vote = election.optionA)
            ElectionOption.OPTION_B -> election.copy(vote = election.optionB)
            ElectionOption.BLANK -> election.copy(vote = "blank")
        }

        try {
            val contract = WalletService.createContract(electionWithVote, identity.pkh()!!)
            val coin = electrumAPI.getOrCreateContractCoin(chain, identity, contract)
            val castInput = BCHinput(chain, coin, BCHscript(chain))
            val castTx = contract.castVote(
                chain, castInput, identity,
                voteOption
            )
            val voteTransactionId = electrumAPI.broadcast(castTx)
            electionRepository.saveElectionsToDisk(listOf(electionWithVote))
            viewModelScope.launch(Dispatchers.Main) {
                state.value = ElectionDetailState.HasVoted(electionWithVote, voteTransactionId)
            }
        } catch (error: Exception) {
            viewModelScope.launch(Dispatchers.Main) {
                state.value = ElectionDetailState.ErrorState(error.message ?: "ERROR!")
            }
            return@launch
        }
        return@launch
    }

    class ElectionDetailViewModelFactory(
        private val election: Election,
        private val walletService: WalletService,
        private val electionRepository: ElectionRepository,
        private val electrumRepository: ElectrumAPI
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ElectionDetailViewModel::class.java))
                return ElectionDetailViewModel(
                    election,
                    walletService,
                    electionRepository,
                    electrumRepository
                ) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}
