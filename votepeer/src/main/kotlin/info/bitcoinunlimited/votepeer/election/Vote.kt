package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.BCHtransaction

data class Vote(
    val option: ElectionOption,
    val voteTransaction: BCHtransaction
)
