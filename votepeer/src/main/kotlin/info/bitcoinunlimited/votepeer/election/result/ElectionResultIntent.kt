package info.bitcoinunlimited.votepeer.election.result

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class ElectionResultIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val refreshResults: MutableStateFlow<Event<RefreshResults?>> = MutableStateFlow(Event(null))
) {
    object RefreshResults
}
