package info.bitcoinunlimited.votepeer.election.master

import info.bitcoinunlimited.votepeer.election.Election

sealed class ElectionMasterViewState {
    object EmptyNoData : ElectionMasterViewState()

    data class LoadingSpinner(
        val loading: Boolean = false
    ) : ElectionMasterViewState()

    data class Elections(
        val elections: List<Election>
    ) : ElectionMasterViewState()

    data class LoadingBlockchainData(
        val data: Elections,
    ) : ElectionMasterViewState()

    data class ElectionMasterError(
        val exception: Exception
    ) : ElectionMasterViewState()
}
