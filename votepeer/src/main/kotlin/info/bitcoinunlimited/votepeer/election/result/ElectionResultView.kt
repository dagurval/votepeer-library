package info.bitcoinunlimited.votepeer.election.result

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface ElectionResultView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Intent to vote
     *
     * @return A flow that emits the vote
     */
    fun updateResults(): Flow<Event<ElectionResultIntent.RefreshResults?>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: ElectionResultState)
}
