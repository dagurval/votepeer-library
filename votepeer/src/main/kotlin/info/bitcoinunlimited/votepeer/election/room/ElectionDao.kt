package info.bitcoinunlimited.votepeer.election.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import info.bitcoinunlimited.votepeer.election.Election
import kotlinx.coroutines.flow.Flow

@Dao
interface ElectionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(elections: List<Election>)

    @Query("SELECT * FROM election_table ORDER BY end_height")
    fun getMainFeedRoom(): List<Election>

    @Query("SELECT * FROM election_table where id == :electionId")
    fun getElection(electionId: String): Election

    @Query("SELECT * FROM election_table where id == :electionId")
    fun observeElection(electionId: String): Flow<Election>

    @Query("DELETE FROM election_table")
    fun clearElections()
}
