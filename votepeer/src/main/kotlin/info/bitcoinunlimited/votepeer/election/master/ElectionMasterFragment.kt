package info.bitcoinunlimited.votepeer.election.master

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentElectionMasterBinding
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.CONTENT_RECYCLER_VIEW_POSITION
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterFragment : Fragment(), ElectionMasterView {
    private lateinit var viewModel: ElectionMasterViewModel
    private lateinit var binding: FragmentElectionMasterBinding
    private lateinit var adapter: ElectionMasterAdapter
    private val intent = ElectionMasterViewIntent()
    private var savedRecyclerPosition: Int = 0

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(
            CONTENT_RECYCLER_VIEW_POSITION,
            (binding.electionRecyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        )
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null)
            savedRecyclerPosition = savedInstanceState.getInt(CONTENT_RECYCLER_VIEW_POSITION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val privateKey = activity?.intent?.getByteArrayExtra("privateKey")
            ?: throw Exception("Cannot get privateKey from intent")
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, privateKey)

        val viewModel: ElectionMasterViewModel by activityViewModels {
            InjectorUtils.provideElectionMasterViewModelFactory(this, payDestination)
        }
        this.viewModel = viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentElectionMasterBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.toolbarError.visibility = View.GONE
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        viewModel.bindIntents(this)

        initSwipeListener()
    }

    private var noElectionsDialogDisplayed = false
    override fun onResume() {
        super.onResume()
        noElectionsDialogDisplayed = false
        viewModel.observeElectrumConnectionStatus()
        viewModel.observeAuthState()
    }

    private fun initAdapter() {
        adapter = ElectionMasterAdapter()

        binding.electionRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.electionRecyclerView.adapter = adapter
    }

    private fun initSwipeListener() {
        binding.swipeToRefresh.setOnRefreshListener {
            intent.swipeToRefresh.value = Event(ElectionMasterViewIntent.SwipeToRefresh)
        }
    }

    override fun initState() = intent.initState
    override fun initElectrumApiState() = intent.initElectrumApiState
    override fun initAuthState() = intent.initAuthState
    override fun refreshSwiped() = intent.swipeToRefresh

    override fun render(state: ElectionMasterViewState) {
        when (state) {
            is LoadingSpinner -> { displaySpinningLoadingIndicator(state.loading) }
            is LoadingBlockchainData -> { renderLoadingBlockchainData(state) }
            is Elections -> { renderDataState(state) }
            is EmptyNoData -> { renderEmptyNoDataState() }
            is ElectionMasterError -> { renderErrorState(state) }
        }

        if (state is LoadingBlockchainData) {
            displayLoadingFromBlockchainIndicator(true)
        } else {
            displayLoadingFromBlockchainIndicator(false)
        }
    }

    private fun renderEmptyNoDataState() {
        displayEmptyFeedView(true)
        displaySpinningLoadingIndicator(false)

        activity?.let {
            if (!noElectionsDialogDisplayed) {
                noElectionsDialogDisplayed = true
                MaterialAlertDialogBuilder(it)
                    .setTitle(resources.getString(R.string.no_elections_found))
                    .setMessage("Create an election at https://voter.cash")
                    .setNeutralButton(resources.getString(R.string.got_it)) { _, _ ->
                        noElectionsDialogDisplayed = false
                    }
            }
        }
    }

    private fun renderLoadingBlockchainData(state: LoadingBlockchainData) {
        val elections = state.data.elections
        displayEmptyFeedView(false)
        adapter.submitList(elections)
    }

    private fun renderDataState(state: Elections) {
        displayEmptyFeedView(false)
        displaySpinningLoadingIndicator(false)
        val elections = state.elections
        adapter.submitList(elections)
    }

    private fun renderErrorState(state: ElectionMasterError) {
        displaySpinningLoadingIndicator(false)
        binding.toolbarError.text = state.exception.message
    }

    override fun renderElectrumApiState(state: ElectrumApiConnectionState) {
        val electrumConnectionStatusToolbar = binding.toolbarElectrumApiConnectionStatus

        context ?: return

        when (state) {
            is ElectrumApiConnectionState.Disconnected -> {
                val message = state.message
                electrumConnectionStatusToolbar.text = message
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorAccent))
            }
            is ElectrumApiConnectionState.Connecting -> {
                val host = state.host
                val port = state.port
                electrumConnectionStatusToolbar.text = "Connecting to Electrum at: $host:$port"
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorOrange))
            }
            is ElectrumApiConnectionState.Connected -> {
                val host = state.host
                val port = state.port

                electrumConnectionStatusToolbar.text = "Connected to Electrum at: $host:$port"
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            }
            is ElectrumApiConnectionState.ElectrumApiError -> {
                val exception = state.exception
                electrumConnectionStatusToolbar.text = "ERROR: $exception"
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorAccent))
            } else -> {
                electrumConnectionStatusToolbar.setBackgroundColor(resources.getColor(R.color.colorOrange))
            }
        }
    }

    private fun displayLoadingFromBlockchainIndicator(display: Boolean) {
        if (display) {
            binding.loadingFromBlockchainProgressBar.visibility = View.VISIBLE
            binding.loadingFromBlockchainText.visibility = View.VISIBLE
        } else {
            binding.loadingFromBlockchainProgressBar.visibility = View.GONE
            binding.loadingFromBlockchainText.visibility = View.GONE
        }
    }

    private fun displaySpinningLoadingIndicator(display: Boolean) {
        binding.swipeToRefresh.isRefreshing = display
    }

    private fun displayEmptyFeedView(display: Boolean) {
        if (display)
            activity?.findViewById<ConstraintLayout>(R.id.emptyFeedView)?.visibility = View.VISIBLE
        else
            activity?.findViewById<ConstraintLayout>(R.id.emptyFeedView)?.visibility = View.GONE
    }

    override fun renderAuthState(state: AuthState) {
        if (state is AuthState.AuthError) {
            renderErrorState(ElectionMasterError(Exception("AuthError")))
        }

        if (state is AuthState.UnAuthenticated || state is AuthState.Authenticating || state is AuthState.AuthenticatedAnonymously) {
            renderAuthLoadingIndicator(true)
        } else if (state is AuthState.Authenticated || state is AuthState.AuthError) {
            renderAuthLoadingIndicator(false)
        }
    }

    private fun renderAuthLoadingIndicator(display: Boolean) {
        if (display) {
            binding.authenticatingProgressBar.visibility = View.VISIBLE
            binding.authenticatingText.visibility = View.VISIBLE
        } else {
            binding.authenticatingProgressBar.visibility = View.GONE
            binding.authenticatingText.visibility = View.GONE
        }
    }
}
