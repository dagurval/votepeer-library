package info.bitcoinunlimited.votepeer.election.result

import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.Tally

sealed class ElectionResultState {
    data class LoadingTally(
        val election: Election
    ) : ElectionResultState()

    data class LoadingWithData(
        val election: Election
    ) : ElectionResultState()

    data class HasTally(
        val election: Election,
        val tally: Tally
    ) : ElectionResultState()

    data class Error(
        val message: String
    ) : ElectionResultState()
}
