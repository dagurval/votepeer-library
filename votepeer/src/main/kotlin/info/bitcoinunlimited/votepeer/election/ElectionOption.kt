package info.bitcoinunlimited.votepeer.election

enum class ElectionOption { OPTION_A, OPTION_B, BLANK }
