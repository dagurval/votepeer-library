package info.bitcoinunlimited.votepeer.election.detail

import info.bitcoinunlimited.votepeer.election.ElectionOption
import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class ElectionDetailIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val submitVote: MutableStateFlow<Event<SubmitVote?>> = MutableStateFlow(Event(null))
) {
    data class SubmitVote(
        val vote: ElectionOption
    )
}
