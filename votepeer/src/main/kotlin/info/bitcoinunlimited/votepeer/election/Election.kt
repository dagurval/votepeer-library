package info.bitcoinunlimited.votepeer.election

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import bitcoinunlimited.libbitcoincash.TwoOptionVote
import info.bitcoinunlimited.votepeer.election.room.ElectionConverters
import java.io.Serializable
import java.util.Date

@Entity(tableName = "election_table")
data class Election(
    @PrimaryKey var id: String,
    val adminAddress: String,
    val optionA: String,
    val optionB: String,
    val description: String,
    @ColumnInfo(name = "end_height") val endHeight: Long,
    val salt: String,

    @ColumnInfo(name = "participants")
    @TypeConverters(ElectionConverters::class)
    val participants: Array<ByteArray>,

    @ColumnInfo(name = "created_at") val createdAt: Long,
    @ColumnInfo(name = "modified_at") val modifiedAt: Long,

    var timeUntilEnd: String = "",
    var vote: String = "",
    var votingError: String = "",
    val blank: String = "beefffffffffffffffffffffffffffffffffffff",
    val voteSpoilt: Boolean = false
) : Serializable {
    fun getTitle(): String {
        val maxTitleLength = 40
        val title = description.split("\n")[0]
        if (title.length > maxTitleLength) {
            return title.subSequence(0, Integer.min(title.length, maxTitleLength)).toString() + "..."
        }
        return title
    }

    /**
     * Pretty string of who hosts the election
     */
    fun getHostedBy(): String {
        val prefixLen = "bitcoincash:".length
        var stringBuffer = StringBuffer(adminAddress.subSequence(prefixLen, prefixLen + 10))
        stringBuffer.append("...")
        stringBuffer.append(adminAddress.subSequence(adminAddress.length - 6, adminAddress.length))
        var trimmedAddress = stringBuffer.toString()
        return "Hosted by $trimmedAddress"
    }

    fun getOptionAHash(): ByteArray {
        return TwoOptionVote.hash160_salted(salt.toByteArray(), optionA.toByteArray())
    }

    fun getOptionBHash(): ByteArray {
        return TwoOptionVote.hash160_salted(salt.toByteArray(), optionB.toByteArray())
    }

    // Required by firestore
    constructor() : this("", "", "", "", "", -1, "", arrayOf(), 1L, 1L)

    val createdDate: Date get() {
        return Date(createdAt * 1000)
    }
}
