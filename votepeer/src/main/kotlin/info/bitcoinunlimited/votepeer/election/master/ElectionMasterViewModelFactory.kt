package info.bitcoinunlimited.votepeer.election.master

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.election.ElectionRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val electionRepository: ElectionRepository,
    private val authRepository: AuthRepository,
    private val walletService: WalletService,
    private val electrumApi: ElectrumAPI,
    private val payDestination: PayDestination
) : AbstractSavedStateViewModelFactory(owner, null) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
        ElectionMasterViewModel(electionRepository, authRepository, walletService, electrumApi, payDestination) as T
}
