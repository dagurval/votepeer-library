package info.bitcoinunlimited.votepeer.election.result

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentElectionResultBinding
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionResultFragment : Fragment(), ElectionResultView {
    private lateinit var binding: FragmentElectionResultBinding
    private val safeArgs: ElectionResultFragmentArgs by navArgs()
    private val intent = ElectionResultIntent()
    val viewModel: ElectionResultViewModel by viewModels {
        InjectorUtils.provideElectionResultViewModelFactory(
            safeArgs.election
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentElectionResultBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        viewModel.bindIntents(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchElectionResultNetwork()
    }

    private fun initClickListeners() {
        binding.electionResultLoadingBar.setOnClickListener {
            intent.refreshResults.value = Event(ElectionResultIntent.RefreshResults)
        }
    }

    override fun initState() = intent.initState
    override fun updateResults() = intent.refreshResults
    override fun render(state: ElectionResultState) {
        when (state) {
            is ElectionResultState.LoadingTally -> renderLoadingState(state)
            is ElectionResultState.LoadingWithData -> renderLoadingWithDataState(state)
            is ElectionResultState.HasTally -> renderHasDataState(state)
            is ElectionResultState.Error -> renderErrorState(state)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun renderLoadingState(state: ElectionResultState.LoadingTally) {
        binding.electionResultLoadingBar.visibility = View.VISIBLE

        binding.resultsOptionALabel.text = state.election.optionA
        binding.resultsOptionBLabel.text = state.election.optionB
        binding.resultsOptionA.text = "Loading..."
        binding.resultsOptionB.text = "Loading..."
        binding.resultsBlank.text = "Loading..."
        binding.resultsNotVoted.text = "Loading..."
        binding.resultsSpoilt.text = "Loading..."

        if (state.election.votingError.isNotEmpty()) {
            renderErrorState(ElectionResultState.Error(state.election.votingError))
        }
    }

    private fun renderLoadingWithDataState(state: ElectionResultState.LoadingWithData) {
        binding.electionResultLoadingBar.visibility = View.VISIBLE
    }

    private fun renderHasDataState(state: ElectionResultState.HasTally) {
        val optionATally = state.tally.optionA.toString()
        val optionBTally = state.tally.optionB.toString()
        val blankTally = state.tally.blank.toString()

        binding.electionResultLoadingBar.visibility = View.GONE
        binding.resultsOptionA.text = optionATally
        binding.resultsOptionB.text = optionBTally
        binding.resultsBlank.text = blankTally
        binding.resultsNotVoted.text = state.tally.didNotVote.toString()
        binding.resultsSpoilt.text = state.tally.blank.toString()
    }

    private fun renderErrorState(state: ElectionResultState.Error) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.something_went_wrong))
            .setMessage(state.message)
            .setNeutralButton(resources.getString(R.string.got_it)) { _, _ ->
                // Do nothing.
            }
            .show()
        binding.electionResultLoadingBar.visibility = View.GONE
    }
}
