package info.bitcoinunlimited.votepeer.election.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentElectionDetailBinding
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionOption.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.election.Vote
import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailIntent.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.Event
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailFragment : Fragment(), ElectionDetailView {
    private lateinit var binding: FragmentElectionDetailBinding
    private val safeArgs: ElectionDetailFragmentArgs by navArgs()
    private val intent = ElectionDetailIntent()

    val viewModel: ElectionDetailViewModel by viewModels {
        InjectorUtils.provideElectionDetailViewModelFactory(
            safeArgs.election,
            this,
            activity?.intent?.getByteArrayExtra("privateKey")
                ?: throw Exception("Cannot get privateKey from intent")
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentElectionDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        viewModel.bindIntents(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.checkElectionState()
    }

    private fun initClickListeners() {
        binding.electionDetailSubmitVote.setOnClickListener {
            val option = when (binding.electionRadioGroup.checkedRadioButtonId) {
                binding.electionOptionAButton.id -> {
                    OPTION_A
                }
                binding.electionOptionBButton.id -> {
                    OPTION_B
                }
                binding.electionBlankButton.id -> {
                    BLANK
                } else -> {
                    throw Exception("Cannot find radiobutton id that was selected when voting in ElectionDetailFragment")
                }
            }

            intent.submitVote.value = Event(SubmitVote(option))
        }

        binding.electionResultButton.setOnClickListener {
            val election = safeArgs.election
            val electionResultDirections = ElectionDetailFragmentDirections
                .actionElectionDetailFragmentToElectionResultFragment(election.getTitle(), election)
            findNavController().navigate(electionResultDirections)
        }
    }

    override fun initState() = intent.initState
    override fun submitVote() = intent.submitVote
    override fun render(state: ElectionDetailState) {
        when (state) {
            is ElectionDetailIsLoading -> renderLoadingState(state)
            is IsVoting -> renderIsVotingState(state)
            is CastingVote -> renderCastingVoteState()
            is HasVoted -> renderHasVoted(state)
            is VoteExpired -> renderVoteExpired(state)
            is VoteSpoilt -> renderVoteSpoilt()
            is ErrorState -> renderErrorState(state)
        }

        if (state is VoteSpoilt) {
            displayVoteSpoiltText(true)
        } else {
            displayVoteSpoiltText(false)
        }
    }

    private fun renderLoadingState(state: ElectionDetailIsLoading) {
        binding.progressBar.visibility = View.VISIBLE
        binding.electionRadioGroup.visibility = View.GONE
        setViewText(state.election)
        initRadioGroup(state.election)
        setElectionRadioGroup(enabled = false)
        setSubmitButton(enabled = false)
        displayHasVoted(false)
    }

    private fun renderIsVotingState(state: IsVoting) {
        setElectionRadioGroup(enabled = true)
        setViewText(state.election)
        initRadioGroup(state.election)
        setSubmitButton(enabled = true)
        displayHasVoted(false)
        binding.progressBar.visibility = View.GONE
    }

    private fun renderCastingVoteState() {
        binding.progressBar.visibility = View.VISIBLE
        setElectionRadioGroup(enabled = false)
        setSubmitButton(enabled = false)
    }

    @SuppressLint("SetTextI18n")
    private fun renderHasVoted(state: HasVoted) {
        initRadioGroup(state.election)
        setElectionRadioGroup(enabled = false)
        setSubmitButton(enabled = false)
        displayHasVoted(true)

        binding.progressBar.visibility = View.GONE
        binding.viewVoteInBlockExplorerButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://explorer.bitcoinunlimited.info/tx/${state.voteTransactionId}")
            startActivity(intent)
        }
        binding.hasVotedText.text = "You voted ${getYouHaveVotedText(state.election)} " +
            "this election in transaction ${state.voteTransactionId}"
    }

    private fun renderVoteExpired(state: VoteExpired) {
        val hasVoted = state.hasVoted
        state.vote?.let { setRadioGroup(it) }
        setElectionRadioGroup(enabled = false)
        setSubmitButton(enabled = false)
        binding.progressBar.visibility = View.GONE
        binding.voteExpiredText.visibility = View.VISIBLE
        if (hasVoted) {
            displayHasVoted(false)
        } else {
            binding.hasNotVotedText.visibility = View.VISIBLE
        }
    }

    private fun renderVoteSpoilt() {
        setElectionRadioGroup(enabled = false)
        setSubmitButton(enabled = false)
        displayHasVoted(true)
        binding.progressBar.visibility = View.GONE
    }

    private fun renderErrorState(state: ErrorState) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle(resources.getString(R.string.something_went_wrong))
            .setMessage(state.message)
            .setNeutralButton(resources.getString(R.string.got_it)) { dialog, which ->
                // Do nothing.
            }
            .show()
        binding.progressBar.visibility = View.GONE
    }

    private fun displayVoteSpoiltText(display: Boolean) {
        if (display) {
            binding.voteSpoiltText.visibility = View.VISIBLE
        } else {
            binding.voteSpoiltText.visibility = View.GONE
        }
    }

    private fun displayHasVoted(display: Boolean) {
        if (display) {
            binding.hasVotedText.visibility = View.VISIBLE
            binding.viewVoteInBlockExplorerButton.visibility = View.VISIBLE
        } else {
            binding.hasVotedText.visibility = View.GONE
            binding.viewVoteInBlockExplorerButton.visibility = View.GONE
        }
    }

    private fun setViewText(election: Election) {
        binding.electionDetailTitle.text = election.getTitle()
        binding.electionDescriptionDetailText.text = election.description
        binding.electionOptionAButton.text = election.optionA
        binding.electionOptionBButton.text = election.optionB
    }

    private fun getYouHaveVotedText(election: Election): String {
        return when {
            election.optionA == election.vote -> {
                election.optionA
            }
            election.optionB == election.vote -> {
                election.optionB
            }
            election.blank == election.vote -> {
                election.blank
            }
            else -> {
                "Nothing"
            }
        }
    }

    private fun initRadioGroup(election: Election) {
        when {
            election.optionA == election.vote -> {
                binding.electionRadioGroup.check(R.id.electionOptionAButton)
            }
            election.optionB == election.vote -> {
                binding.electionRadioGroup.check(R.id.electionOptionBButton)
            }
            election.blank == election.vote -> {
                binding.electionRadioGroup.check(R.id.electionBlankButton)
            }
        }
        binding.electionRadioGroup.visibility = View.VISIBLE
    }

    private fun setRadioGroup(vote: Vote) {
        if (vote.option == OPTION_A)
            binding.electionRadioGroup.check(R.id.electionOptionAButton)
        else if (vote.option == OPTION_B)
            binding.electionRadioGroup.check(R.id.electionOptionBButton)
        else if (vote.option == BLANK)
            binding.electionRadioGroup.check(R.id.electionBlankButton)

        binding.electionRadioGroup.visibility = View.VISIBLE
    }

    private fun setElectionRadioGroup(enabled: Boolean) {
        binding.electionRadioGroup.isEnabled = enabled
        binding.electionRadioGroup.forEach {
            it.isEnabled = enabled
        }
    }

    private fun setSubmitButton(enabled: Boolean) {
        binding.electionDetailSubmitVote.isEnabled = enabled
    }
}
