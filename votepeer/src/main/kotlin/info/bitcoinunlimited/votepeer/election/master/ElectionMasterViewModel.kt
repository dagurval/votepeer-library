package info.bitcoinunlimited.votepeer.election.master

import android.util.Log
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionRepository
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.TAG_FIREBASE
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModel(
    private val electionRepository: ElectionRepository,
    private val authRepository: AuthRepository,
    private val walletService: WalletService,
    private val electrumApi: ElectrumAPI,
    private val payDestination: PayDestination,
) : ViewModel() {
    private val state = MutableStateFlow<ElectionMasterViewState?>(null)
    private val electrumApiState = MutableStateFlow<ElectrumApiConnectionState?>(null)
    private val authState = MutableStateFlow<AuthState?>(null)
    private val currentUserId = payDestination.address.toString()
    private val TAG = "ElectionMasterViewModel"

    fun bindIntents(view: ElectionMasterView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.initElectrumApiState().onEach {
            electrumApiState.filterNotNull().collect {
                view.renderElectrumApiState(it)
            }
        }.launchIn(viewModelScope)

        view.initAuthState().onEach {
            authState.filterNotNull().collect {
                view.renderAuthState(it)
            }
        }.launchIn(viewModelScope)

        view.refreshSwiped().onEachEvent { _ ->
            getElectionsDisk()
        }.launchIn(viewModelScope)
    }

    fun observeAuthState() = viewModelScope.launch(Dispatchers.IO + coroutineErrorHandler) {
        authRepository.authState.collect {
            viewModelScope.launch(Dispatchers.Main + coroutineErrorHandler) {
                authState.value = it
            }
        }
    }

    private val coroutineErrorHandler = CoroutineExceptionHandler { _, exception ->
        state.value = ElectionMasterError(Exception(exception))
    }

    init {
        viewModelScope.launch(Dispatchers.IO + coroutineErrorHandler) {
            val authState = authRepository.authState.value
            val isAnonymous = authRepository.isSignedIdAnonymously()
            if (authState is AuthState.AuthenticatedAnonymously) {
                waitForCurrentUserId()
            } else if (!isAnonymous && authState is AuthState.Authenticated) {
                getElectionsDisk()
                electionsNetwork()
                    .collect { electionList ->
                        Log.w(TAG_FIREBASE, electionList.toString())
                        setViewState(electionList)
                    }
            } else {
                waitForCurrentUserId()
            }
        }
    }

    private suspend fun waitForCurrentUserId() {
        authRepository.authState.collect { authState ->
            val isAnonymous = authRepository.isSignedIdAnonymously()

            if (!isAnonymous && authState is AuthState.Authenticated) {
                getElectionsDisk()

                electionsNetwork()
                    .collect { electionList ->
                        Log.w(TAG_FIREBASE, electionList.toString())
                        setViewState(electionList)
                    }
            }
        }
    }

    fun observeElectrumConnectionStatus() {
        electrumApi.connectionState.onEach { connectionState ->
            if (connectionState != null) {
                setElectrumState(connectionState)
            }
            electrumApi.ping()
        }.flowOn(Dispatchers.IO + coroutineErrorHandler).launchIn(viewModelScope)
    }

    private fun setElectrumState(
        connectionState: ElectrumApiConnectionState
    ) = viewModelScope.launch(Dispatchers.Main + coroutineErrorHandler) {
        electrumApiState.value = connectionState
    }

    private fun getElectionsDisk() = viewModelScope.launch(Dispatchers.IO + coroutineErrorHandler) {
        state.value = LoadingSpinner(true)
        val electionsDisk = electionRepository.getElectionsFromDisk()
        setViewState(electionsDisk)
        state.value = LoadingSpinner(false)
        if (electionsDisk.isNotEmpty()) state.value = LoadingBlockchainData(Elections(electionsDisk))
    }

    private fun setViewState(elections: List<Election>) = viewModelScope.launch(Dispatchers.Main) {
        if (elections.isEmpty()) {
            state.value = null
            state.value = EmptyNoData
        } else
            state.value = Elections(elections)
    }

    private suspend fun electionsNetwork(): Flow<List<Election>> = flow {
        if (!authRepository.isSignedIdAnonymously()) {
            viewModelScope.launch(Dispatchers.Main) {
                state.value = LoadingSpinner(true)
            }
            val electionsNetwork = electionRepository.getElectionsNetwork(payDestination.address.toString())
            val electionsNetworkSorted = electionsNetwork.sortedBy { it.endHeight }
            mapElectionsToNetworkMetadata(electionsNetworkSorted).collect {
                electionRepository.clearElections()
                electionRepository.saveElectionsToDisk(it)
                emit(it)
            }
        }
        emit(listOf<Election>())
    }

    private suspend fun mapElectionsToNetworkMetadata(elections: List<Election>): Flow<List<Election>> = channelFlow {
        var isClosed = false
        viewModelScope.launch(Dispatchers.IO) {
            val asyncElections = elections.map {
                launch {
                    it.timeUntilEnd = electrumApi.getEndTimeHuman(it.endHeight)
                    it.vote = getVoteSentence(it)
                }
            }

            val result = asyncElections.joinAll()
            println(result)

            if (!isClosed)
                send(elections)
            else
                return@launch
        }

        awaitClose { isClosed = true }
    }

    suspend fun getInfo(elections: List<Election>) = withContext(Dispatchers.IO) {
        try {
            val asyncelections = elections.map {
                launch {
                    it.timeUntilEnd = electrumApi.getEndTimeHuman(it.endHeight)
                    it.vote = getVoteSentence(it)
                }
            }
            false
        } catch (e: Throwable) {
            true
        }
    }

    private suspend fun getVoteSentence(election: Election): String {
        try {
            val contract = WalletService.createContract(election, walletService.currentUser.pkh()!!)
            val voteTx = electrumApi.fetchVoteTransaction(contract.address(ChainSelector.BCHMAINNET), election.endHeight)
            return walletService.getVoteSentence(election, voteTx)
        } catch (e: Exception) {
            election.votingError = e.message.toString()
            return ""
        }
    }
}
