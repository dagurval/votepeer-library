package info.bitcoinunlimited.votepeer.election.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import info.bitcoinunlimited.votepeer.election.Election

@Database(entities = [Election::class], version = 13, exportSchema = false)
@TypeConverters(ElectionConverters::class)
abstract class ElectionDatabase : RoomDatabase() {
    abstract fun electionDao(): ElectionDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: ElectionDatabase? = null

        fun getInstance(context: Context): ElectionDatabase {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
                }
        }

        private fun buildDatabase(context: Context): ElectionDatabase {
            return Room.databaseBuilder(
                context, ElectionDatabase::class.java,
                "election-db"
            ).fallbackToDestructiveMigration().build()
        }
    }
}
