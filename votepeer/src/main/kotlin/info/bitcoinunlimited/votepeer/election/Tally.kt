package info.bitcoinunlimited.votepeer.election

data class Tally(
    val electionId: String,
    val optionA: Int = 0,
    val optionB: Int = 0,
    val blank: Int = 0,
    val didNotVote: Int = 0,
    val spoilt: Int = 0
)
