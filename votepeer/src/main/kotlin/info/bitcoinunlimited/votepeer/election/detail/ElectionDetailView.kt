package info.bitcoinunlimited.votepeer.election.detail

import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailIntent.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface ElectionDetailView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Intent to vote
     *
     * @return A flow that emits the vote
     */
    fun submitVote(): Flow<Event<SubmitVote?>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: ElectionDetailState)
}
