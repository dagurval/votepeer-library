package info.bitcoinunlimited.votepeer

import android.content.Context
import android.content.Intent
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.auth.network.SignedChallenge
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionOption
import info.bitcoinunlimited.votepeer.election.Vote
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

/**
 * Methods to analyze votes based on the blockchain state.
 */
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class WalletService(
    val currentUser: PayDestination
) {

    fun signChallenge(challenge: String): SignedChallenge {
        val challengeBytes = challenge.toByteArray()
        val signerAddress = currentUser.address.toString()
        val privateKey = currentUser.secret ?: throw Error("Cannot get privateKey / secret")
        val signature = Wallet.signMessage(challengeBytes, privateKey)
        return SignedChallenge(
            challenge,
            signature,
            signerAddress,
            signerAddress
        )
    }

    fun getVoteSentence(election: Election, voteTx: BCHtransaction?): String {

        if (voteTx != null) {
            val voteOptionHash = parseVote(voteTx)

            if (voteOptionHash.contentEquals(election.getOptionAHash())) {
                return "You voted ${election.optionA}"
            }
            if (voteOptionHash.contentEquals(election.getOptionBHash())) {
                return "You voted ${election.optionB}"
            }
            if (voteOptionHash.contentEquals(TwoOptionVoteContract.BLANK_VOTE)) {
                return "You voted blank"
            }
        }

        // User has not vote yet.
        return "You have not voted"
    }

    fun getCurrentUserPkh(): ByteArray {
        return currentUser.pkh() ?: throw Exception("Cannot get currentUserAddress")
    }

    fun getCurrentUserVote(election: Election, voteTx: BCHtransaction): Vote? {
        val contract = createContract(election, this.currentUser.pkh()!!)
        return getVoteOption(contract, election, voteTx)
    }

    @InternalCoroutinesApi
    companion object {
        fun getVoteOption(contract: TwoOptionVoteContract, election: Election, voteTx: BCHtransaction): Vote? {
            val voteOptionHash = parseVote(voteTx)

            if (voteOptionHash.contentEquals(election.getOptionAHash())) {
                return Vote(ElectionOption.OPTION_A, voteTx)
            }
            if (voteOptionHash.contentEquals(election.getOptionBHash())) {
                return Vote(ElectionOption.OPTION_B, voteTx)
            }
            if (voteOptionHash.contentEquals(TwoOptionVoteContract.BLANK_VOTE)) {
                return Vote(ElectionOption.BLANK, voteTx)
            }

            // User has not vote yet.
            return null
        }

        /**
         * Parses the vote option from a transaction.
         */
        private fun parseVote(tx: BCHtransaction): ByteArray {
            if (tx.inputs.size > 1) {
                error("Multiple inputs NYI")
            }
            val script = tx.inputs[0].script.flatten()
            var pos = 0
            val msgSigSize = script[pos++]
            if (!isPlausibleSignatureSize(msgSigSize.toInt())) {
                error("Unexpected message siganture size ${msgSigSize.toInt()}")
            }
            pos += msgSigSize
            if (script[pos++] != 40.toByte()) {
                error("Expected push 40")
            }
            // Next 40 bytes is the message, which is (election id + vote). Copy out the vote.
            return script.copyOfRange(pos + 20, pos + 40)
        }

        /**
         * If size is a valid signature size.
         */
        private fun isPlausibleSignatureSize(signatureSize: Int): Boolean {
            return signatureSize in 64..73
        }

        fun getVotePeerIntent(applicationContext: Context, applicationId: String, setSupportActionBar: Boolean): Intent {
            val entropy = GenerateEntropy(128)
            val mnemonic = GenerateBip39SecretWords(entropy)
            val walletDb = OpenKvpDB(PlatformContext(applicationContext), applicationId + "voter.cash.bip44walletdb")
                ?: throw Exception("Cannot init wallet! OpenKvpDB returns null in initWallet()")

            val wallet = Bip44Wallet(
                walletDb,
                "my-wallet",
                ChainSelector.BCHMAINNET,
                mnemonic
            )

            val payDestination = wallet.getDestinationAtIndex(0)

            val votePeerIntent = Intent(applicationContext, VotePeerActivity::class.java)
            votePeerIntent.putExtra("privateKey", payDestination.secret)
            votePeerIntent.putExtra("setSupportActionBar", setSupportActionBar)
            return votePeerIntent
        }

        fun createContract(election: Election, participant: ByteArray): TwoOptionVoteContract {
            val salt: ByteArray = election.salt.toByteArray()
            val optAHash = TwoOptionVote.hash160_salted(salt, election.optionA.toByteArray())
            val optBHash = TwoOptionVote.hash160_salted(salt, election.optionB.toByteArray())

            val proposalID = TwoOptionVote.calculate_proposal_id(
                salt, election.description, election.optionA, election.optionB,
                election.endHeight.toInt(), election.participants
            )

            return TwoOptionVoteContract(proposalID, optAHash, optBHash, participant)
        }
    }
}
