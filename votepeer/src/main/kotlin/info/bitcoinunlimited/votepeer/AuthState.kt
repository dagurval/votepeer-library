package info.bitcoinunlimited.votepeer

sealed class AuthState {
    @Suppress("unused")
    object UnAuthenticated : AuthState()

    @Suppress("unused")
    object Authenticating : AuthState()

    data class AuthenticatedAnonymously(
        val userId: String,
    ) : AuthState()

    data class Authenticated(
        val userId: String,
    ) : AuthState()

    data class AuthError(
        val exception: Exception
    ) : AuthState()
}
