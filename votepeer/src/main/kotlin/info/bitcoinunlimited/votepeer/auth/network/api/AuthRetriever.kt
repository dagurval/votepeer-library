package info.bitcoinunlimited.votepeer.auth.network.api

import com.google.gson.internal.LinkedTreeMap
import info.bitcoinunlimited.votepeer.auth.network.IdentifyRequest
import info.bitcoinunlimited.votepeer.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthRetriever {
    private val service: AuthApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.EndpointUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(AuthApi::class.java)
    }

    suspend fun identify(request: IdentifyRequest): LinkedTreeMap<String, String> {
        val operation = request.operation
        val cookie = request.cookie
        val address = request.address
        val signature = request.signature
        return service.identify(operation, cookie, address, signature)
    }
}
