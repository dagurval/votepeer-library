package info.bitcoinunlimited.votepeer.auth.network

data class QrCodeData(
    val op: String,
    val chal: String,
    val cookie: String
)
