package info.bitcoinunlimited.votepeer.auth.network

import java.util.Date

data class JsonWebToken(
    val value: String,
    val issuedAt: Date
)

data class SignInAnonymouslyResult(
    val userId: String?,
    val error: Exception?
)
