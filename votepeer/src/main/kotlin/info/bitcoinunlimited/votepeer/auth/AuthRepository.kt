package info.bitcoinunlimited.votepeer.auth

import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import info.bitcoinunlimited.votepeer.AuthState
import info.bitcoinunlimited.votepeer.WalletService
import info.bitcoinunlimited.votepeer.auth.network.IdentifyRequest
import info.bitcoinunlimited.votepeer.auth.network.JsonWebToken
import info.bitcoinunlimited.votepeer.auth.network.QrCodeData
import info.bitcoinunlimited.votepeer.auth.network.SignedChallenge
import info.bitcoinunlimited.votepeer.auth.network.api.AuthRetriever
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.Constants.FirebaseRegion
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import java.util.Date
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.tasks.await
import retrofit2.HttpException

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class AuthRepository(private val walletService: WalletService) {
    val authState = MutableStateFlow<AuthState?>(null)

    private fun functions(): FirebaseFunctions {
        return FirebaseFunctions.getInstance(FirebaseRegion)
    }

    fun auth() = FirebaseAuth.getInstance()

    fun observeAuth() = GlobalScope.launch(Dispatchers.IO) {
        auth().addAuthStateListener {
            it.currentUser?.uid?.let { currentUserId ->
                if (isSignedIdAnonymously()) {
                    authState.value = AuthState.AuthenticatedAnonymously(currentUserId)
                } else if (isSignedInWithWallet() && !isSignedIdAnonymously())
                    authState.value = AuthState.Authenticated(currentUserId)
            }
        }
    }

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        authState.value = AuthState.AuthError(Exception(exception))
    }

    suspend fun identify(signedChallenge: SignedChallenge, qrCodeData: QrCodeData) = suspendCoroutine<JsonWebToken> {
        val identifyRequest = IdentifyRequest(
            qrCodeData.op,
            qrCodeData.cookie,
            signedChallenge.signerAddress,
            signedChallenge.signatureBase64
        )

        val errorHandler = CoroutineExceptionHandler { _, exception ->
            authState.value = AuthState.AuthError(Exception(exception))
            when (exception) {
                is HttpException -> {

                    // NOTE: Workaround until signing is resolved
                    if (exception.code() == 418) {
                        throw exception
                    } else {
                        throw exception
                    }
                }
                else -> {
                    throw exception
                }
            }
        }

        GlobalScope.launch(errorHandler) {
            val result = AuthRetriever().identify(identifyRequest)
            val token = result["token"] ?: throw Exception("Cannot get jsonWebToken from identify:response")

            val jsonWebToken = JsonWebToken(
                token,
                Date()
            )
            it.resume(jsonWebToken)
        }
    }

    private suspend fun identify(signedChallenge: SignedChallenge) = suspendCoroutine<JsonWebToken> { continuation ->
        val cookie = auth().currentUser?.uid ?: throw Exception("Cannot get currentUserId")
        val identifyRequest = IdentifyRequest(
            "login",
            cookie,
            signedChallenge.signerAddress,
            signedChallenge.signatureBase64
        )

        val errorHandler = CoroutineExceptionHandler { _, exception ->
            authState.value = AuthState.AuthError(Exception(exception))

            when (exception) {
                is HttpException -> {

                    // NOTE: Workaround until signing is resolved
                    if (exception.code() == 418) {
                        println("exception.code() == 418")
                    } else {
                        println(exception)
                        // TODO: Handle exception
                    }
                }
                else -> {
                    println(exception)
                    throw exception
                }
            }
        }

        GlobalScope.launch(errorHandler) {
            val result = AuthRetriever().identify(identifyRequest)
            val token = result["token"] ?: throw Exception("Cannot get jsonWebToken from identify:response")

            val jsonWebToken = JsonWebToken(
                token,
                Date()
            )
            continuation.resume(jsonWebToken)
        }
    }

    fun getCurrentUserId(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    private suspend fun requestChallenge() = suspendCoroutine<String> { continuation ->
        functions().getHttpsCallable(Constants.RequestChallenge)
            .call()
            .continueWith { task ->
                if (task.isSuccessful) {
                    task.result?.data?.let {
                        continuation.resume(it as String)
                    }
                } else {
                    val exception = task.exception
                    authState.value = AuthState.AuthError(Exception(exception))
                    when (exception) {
                        is FirebaseFunctionsException -> {
                            continuation.resumeWithException(exception)
                        }
                        is FirebaseNetworkException -> {
                            continuation.resumeWithException(exception)
                        }
                        else -> {
                            continuation.resumeWithException(Throwable(exception))
                        }
                    }
                }
            }
    }

    fun isSignedIdAnonymously(): Boolean {
        return auth().currentUser?.isAnonymous ?: false
    }

    private fun isSignedInWithWallet(): Boolean {
        return auth().currentUser?.uid?.startsWith("bitcoincash") == true
    }

    private suspend fun signInWithCustomJwt(jsonWebToken: JsonWebToken) {
        try {
            val customAuthResult = auth().signInWithCustomToken(jsonWebToken.value)
                .addOnFailureListener {
                    authState.value = AuthState.AuthError(Exception(it))
                    throw it
                }
                .await()

            val customAuthCredential = customAuthResult.credential ?: return
            auth().signOut()
            val currentUserId = auth().signInWithCredential(customAuthCredential).await().user?.uid
                ?: throw Exception("Cannot login!")
            authState.value = AuthState.Authenticated(currentUserId)
            return
        } catch (error: Exception) {
            authState.value = AuthState.AuthError(Exception(error))
            throw error
        }
    }

    @Suppress("unused")
    suspend fun signIn() {
        auth().signOut()
        auth().signInAnonymously().await()
        if (isSignedIdAnonymously()) {
            val challenge = requestChallenge()
            val signedChallenge = walletService.signChallenge(challenge)
            val jsonWebToken: JsonWebToken = identify(signedChallenge)
            signInWithCustomJwt(jsonWebToken)
        } else {
            val error = Exception("Not authenticated anonymously")
            authState.value = AuthState.AuthError(error)
            throw error
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: AuthRepository? = null

        fun getInstance(walletService: WalletService) = instance ?: synchronized(this) {
            instance
                ?: AuthRepository(walletService)
                    .also { instance = it }
        }
    }
}
