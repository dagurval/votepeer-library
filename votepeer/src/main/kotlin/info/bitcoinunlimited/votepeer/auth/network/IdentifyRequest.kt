package info.bitcoinunlimited.votepeer.auth.network

data class IdentifyRequest(
    val operation: String,
    val cookie: String,
    val address: String,
    val signature: String
)
