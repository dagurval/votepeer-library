package info.bitcoinunlimited.votepeer.auth.network

import bitcoinunlimited.libbitcoincash.Codec

data class SignedChallenge(
    val challenge: String,
    val signature: ByteArray,
    val signerAddress: String,
    val cookie: String,
    val signatureBase64: String = Codec.encode64(signature)
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SignedChallenge

        if (challenge != other.challenge) return false
        if (!signature.contentEquals(other.signature)) return false
        if (signerAddress != other.signerAddress) return false
        if (signatureBase64 != other.signatureBase64) return false

        return true
    }

    override fun hashCode(): Int {
        var result = challenge.hashCode()
        result = 31 * result + signature.contentHashCode()
        result = 31 * result + signerAddress.hashCode()
        result = 31 * result + signatureBase64.hashCode()
        return result
    }
}
