import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    kotlin("plugin.serialization")
    `maven-publish`
}

android {
    compileSdkVersion(30)

    defaultConfig {
        /*testApplicationId = "info.bitcoinunlimited.votepeer"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        setTestFunctionalTest(true)*/

        minSdkVersion(26)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            // minifyEnabled(false)
        }
        getByName("release") {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            // minifyEnabled(false)
        }
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    sourceSets {
        all {
            java.srcDir("src/$name/kotlin")
        }
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
        dataBinding = true
    }
}

dependencies {
    val lifecycleVersion = "2.2.0"
    val navigationVersion = "2.3.1"
    val roomVersion = "2.2.5"

    implementation("info.bitcoinunlimited", "libbitcoincash", "0.2.3")
    // implementation(files("/Users/jqrgen/dev/bu/libbitcoincashkotlin/libbitcoincash/build/outputs/aar/libbitcoincash-debug.aar"))

    implementation("androidx.core", "core-ktx", "1.3.2")
    implementation("androidx.appcompat", "appcompat", "1.2.0")
    implementation("com.google.android.material", "material", "1.2.1")

    // Kotlin
    implementation(kotlin("stdlib-jdk7"))
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", "1.0.0-RC")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-android", "1.3.9")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-play-services", "1.3.3")

    // Android
    implementation("androidx.constraintlayout", "constraintlayout", "2.0.4")
    implementation("androidx.lifecycle", "lifecycle-extensions", lifecycleVersion)
    implementation("androidx.lifecycle", "lifecycle-viewmodel-ktx", lifecycleVersion)
    implementation("androidx.navigation", "navigation-fragment-ktx", navigationVersion)
    implementation("androidx.navigation", "navigation-ui-ktx", navigationVersion)
    implementation("androidx.room", "room-ktx", roomVersion)
    implementation("androidx.room", "room-runtime", roomVersion)
    implementation("androidx.swiperefreshlayout", "swiperefreshlayout", "1.0.0")
    kapt("androidx.room", "room-compiler", roomVersion)
    kapt("com.android.databinding", "compiler", "3.1.4")

    // Firebase
    implementation(platform("com.google.firebase:firebase-bom:26.0.0"))
    implementation("com.google.firebase", "firebase-auth-ktx")
    implementation("com.google.firebase", "firebase-core")
    implementation("com.google.firebase", "firebase-firestore-ktx")
    implementation("com.google.firebase", "firebase-functions-ktx")
    implementation("com.google.firebase", "firebase-messaging")

    // Retrofit HTTP
    implementation("com.google.code.gson", "gson", "2.8.6")
    implementation("com.squareup.okhttp3", "okhttp", "3.12.1")
    implementation("com.squareup.retrofit2", "converter-gson", "2.9.0")
    implementation("com.squareup.retrofit2", "retrofit", "2.9.0")

    // QR Scanner
    implementation("com.journeyapps", "zxing-android-embedded", "4.1.0")

    // QR generator
    implementation("com.google.zxing", "core", "3.4.0")

    testImplementation("junit", "junit", "4.+")
    androidTestImplementation("androidx.test.ext", "junit", "1.1.2")
    androidTestImplementation("androidx.test.espresso", "espresso-core", "3.3.0")
}

// Publishing
group = "info.bitcoinunlimited"
version = "0.4.6"

publishing {
    publications {
        create<MavenPublication>("votepeer") {
            artifact("$buildDir/outputs/aar/votepeer-release.aar")
        }
    }

    repositories {
        maven {
            url = uri("$buildDir/repos")
        }
    }
}

tasks.map {
    if (it.name.startsWith("publish")) {
        // Make sure library has been built when publishing.
        it.dependsOn(tasks.findByName("build")!!)
    }
}

fun Project.getKtlintConfiguration(): Configuration {
    return configurations.findByName("ktlint") ?: configurations.create("ktlint") {
        val dependency = project.dependencies.create("com.pinterest:ktlint:0.37.2")
        dependencies.add(dependency)
    }
}

tasks.register("ktlint", JavaExec::class.java) {
    description = "Check Kotlin code style."
    group = "Verification"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android")
}
tasks.register("ktlintFormat", JavaExec::class.java) {
    description = "Fix Kotlin code style deviations."
    group = "formatting"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android")
}
