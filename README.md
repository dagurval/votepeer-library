# VotePeer Android voting library
[ico-telegram]: votepeer/telegram.svg
[link-telegram]: https://t.me/buip129
[![Chat on Telegram][ico-telegram]][link-telegram]
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/votepeer.svg?style=social&label=Follow%20%40votepeer)](https://twitter.com/votepeer)
[![MIT license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](LICENSE)
[![Website](https://img.shields.io/badge/Website_&_Demo-Online-green.svg)](https://voter.cash/)

## Importing co-dependencies
### Update project-level `build.gradle to import the following
```groovy
buildscript {
    ext {
        kotlin_version = "1.4.21"
        navigation_version = "2.3.2"
    }
    repositories {
        google()
        jcenter()
    }
    dependencies {
        // Android - Gradle
        // NOTE: This version is a bit lower due to INTELLIJ IDEA
        // https://stackoverflow.com/a/65864176/2333802
        classpath "com.android.tools.build:gradle:4.0.2"
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"

        // Navigation
        classpath "androidx.navigation:navigation-safe-args-gradle-plugin:$navigation_version"

        // Firebase
        classpath "com.google.gms:google-services:4.3.5"
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url "https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/repos/"
        }
        maven {
            url "https://nerdekollektivet.gitlab.io/votepeer-library/repos/"
        }
    }
}
```

### Update app-level `build.gradle`
#### Apply libraries
```groovy
plugins {
  id 'com.android.application'
  id 'kotlin-android'
  id 'kotlin-kapt'
  id 'kotlin-android-extensions'
  id 'com.google.gms.google-services'
}
```

#### Import libraries
```groovy
dependencies {

    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    def lifecycle_version = '2.2.0'
    def room_version = "2.2.6"
    def fragment_version = '1.2.5'
    def work_version = "2.0.0"

    implementation "info.bitcoinunlimited:libbitcoincash:0.2.3"
    implementation "info.bitcoinunlimited:votepeer:0.3.6"
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    // Kotlin
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    implementation "org.jetbrains.kotlinx:kotlinx-serialization-cbor:1.0.0-RC"
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.1'
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.3.3'

    // Android
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'androidx.core:core-ktx:1.3.2'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
    implementation "androidx.navigation:navigation-fragment-ktx:$navigation_version"
    implementation "androidx.navigation:navigation-ui-ktx:$navigation_version"
    implementation "androidx.fragment:fragment-ktx:$fragment_version"
    implementation "androidx.work:work-runtime-ktx:$work_version"
    implementation "androidx.room:room-runtime:$room_version"
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:$room_version"

    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-extensions:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"
    kapt "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-reactivestreams-ktx:$lifecycle_version"
    implementation "androidx.paging:paging-runtime-ktx:2.1.2"

    // Firebase
    implementation platform('com.google.firebase:firebase-bom:26.4.0')
    implementation "com.google.firebase:firebase-auth-ktx"
    implementation "com.google.firebase:firebase-core"
    implementation "com.google.firebase:firebase-firestore-ktx"
    implementation "com.google.firebase:firebase-functions-ktx"
    implementation "com.google.firebase:firebase-messaging"

    //noinspection GradleDependency
    implementation "com.google.android.material:material:1.1.0"

    // Retrofit HTTP
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.squareup.okhttp3:okhttp:4.7.2"
    implementation "com.google.code.gson:gson:2.8.6"
    implementation "androidx.cardview:cardview:1.0.0"
    implementation "androidx.recyclerview:recyclerview:1.1.0"

    // Firebase
    implementation platform("com.google.firebase:firebase-bom:26.0.0")
    implementation "com.google.firebase:firebase-auth-ktx"
    implementation "com.google.firebase:firebase-core"
    implementation "com.google.firebase:firebase-firestore-ktx"
    implementation "com.google.firebase:firebase-functions-ktx"

    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
}
```

#### Enable databinding in app-level `build.gradle`
```groovy
android {
    //...
    buildFeatures {
        dataBinding true
    }
}
```

## Importing dependencies from the library
### Add application to `AndroidManifest.xml
```xml
<application
    android:name="info.bitcoinunlimited.votepeerexample.ExampleApplication">
    <!-- ... more stuff here ... -->
</application>
```

### Add VotePeerActivity to `AndroidManifest.xml
```xml
<activity android:name="info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivity" />
```

## Initiating the votepeer android library

### (dependency) Load libbitcoincash library in `Application()`
```kotlin
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ExampleApplication : Application() {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }
}
```
### use `WalletService.getVotePeerIntent(...)`to get intent
```kotlin
fun startVotePeer() = GlobalScope.launch(Dispatchers.Main){
    val intent = WalletService.getVotePeerIntent(applicationContext, BuildConfig.APPLICATION_ID, true)
    startActivity(intent)
}
```

## Setup the android app with firebase
Here we have imported the votepeer android library into our app

The library currently requires firebase to e connect at the app-level to work

### Add `app/google-services.json` to .gitignore
```gitignore
*.iml
.gradle
/local.properties
/.idea/caches
/.idea/libraries
/.idea/modules.xml
/.idea/workspace.xml
/.idea/navEditor.xml
/.idea/assetWizardSettings.xml
.DS_Store
/build
/captures
.externalNativeBuild
.cxx
local.properties
/.idea/misc.xml
/.idea/*

app/google-services.json
```

### Setup Firebase with your Android App
Follow the tutorial below to set up a firebase project and retrieve your `google-services.json` file

https://firebase.google.com/docs/android/setup

add `google-service.json` to: `app/google-services.json

// TODO:
keys/ file

# Run app!
